use lambda_http::{handler, lambda_runtime::{self, Context, Error}, IntoResponse, Request, Response, http::StatusCode, Body};
use lambda_http::RequestExt;
use reqwest::Client;
use serde_json::json;
use aws_sdk_dynamodb::{Client as DynamoClient, types::AttributeValue};

#[tokio::main]
async fn main() -> Result<(), Error> {
    let config = aws_config::load_from_env().await;
    let dynamo_client = DynamoClient::new(&config);
    let client = Client::new();

    lambda_runtime::run(handler(move |event, ctx| {
        let client_clone = client.clone();
        let dynamo_client_clone = dynamo_client.clone();
        async move {
            my_handler(event, ctx, &client_clone, &dynamo_client_clone).await
        }
    }))
    .await?;

    Ok(())
}

async fn my_handler(
    event: Request,
    _ctx: Context,
    client: &Client,
    dynamo_client: &DynamoClient,
) -> Result<impl IntoResponse, Error> {
    let path = event.uri().path();
    let query_params = event.query_string_parameters();
    let input_api_key = query_params.get("api_key").unwrap_or_default();
    match (event.method().as_str(), path) {
        ("POST", "/generate-story") => {

            if let Err(e) = update_api_key_usage(&dynamo_client, input_api_key).await {
                return Ok(Response::builder()
                    .status(StatusCode::INTERNAL_SERVER_ERROR)
                    .body(Body::from("Failed to update API key usage"))
                    .unwrap());
            }
            let res = client.post("https://api.openai.com/v1/chat/completions")
                .header("Authorization", format!("Bearer {}", input_api_key))
                .json(&json!({
                    "model": "gpt-3.5-turbo",
                    "messages": [{"role": "user", "content": "Tell me a short story of 50 words."}],
                    "max_tokens": 100,
                    "temperature": 0.7
                }))
                .send()
                .await;

            match res {
                Ok(response) => {
                    let status = response.status();
                    let body_text = response.text().await.unwrap_or_default();
                    if status.is_success() {
                        Ok(Response::builder()
                            .status(StatusCode::OK)
                            .body(Body::from(body_text))
                            .unwrap())
                    } else {
                        eprintln!("OpenAI API Error: Status: {}, Body: {}", status, body_text);
                        Ok(Response::builder()
                            .status(StatusCode::INTERNAL_SERVER_ERROR)
                            .body(Body::from(format!("Failed to generate story with OpenAI: {}", body_text)))
                            .unwrap())
                    }
                },
                Err(e) => {
                    eprintln!("HTTP Request Failed: {:?}", e);
                    Ok(Response::builder()
                        .status(StatusCode::INTERNAL_SERVER_ERROR)
                        .body(Body::from("Failed to generate story with OpenAI"))
                        .unwrap())
                },
            }
        },
        ("GET", "/get-usage") => {
            if input_api_key.is_empty() {
                return Ok(Response::builder()
                    .status(StatusCode::UNAUTHORIZED)
                    .body(Body::from("API key is missing"))
                    .unwrap());
            }
            let data = scan_for_api_key_usage(&dynamo_client, input_api_key).await?;
            Ok(Response::builder()
                .status(StatusCode::OK)
                .body(Body::from(data))
                .unwrap())
        },
        _ => Ok(Response::builder()
            .status(StatusCode::NOT_FOUND)
            .body(Body::from("Endpoint not found"))
            .unwrap()),
    }
}

async fn update_api_key_usage(dynamo_client: &DynamoClient, api_key: &str) -> Result<(), Error> {
    let table_name = "apisimp"; // Replace with your actual table name

    let update_result = dynamo_client.update_item()
        .table_name(table_name)
        .key("simple", AttributeValue::S(api_key.to_string())) // Use the correct partition key name as per your table schema
        // If you're only using the partition key, ensure the operation aligns with your table's primary key configuration
        .update_expression("SET usage_count = if_not_exists(usage_count, :start) + :inc")
        .expression_attribute_values(":inc", AttributeValue::N("1".to_string()))
        .expression_attribute_values(":start", AttributeValue::N("0".to_string()))
        .return_values(aws_sdk_dynamodb::types::ReturnValue::UpdatedNew)
        .send()
        .await;

    match update_result {
        Ok(_) => Ok(()),
        Err(e) => {
            eprintln!("Failed to update API key usage: {:?}", e);
            Err(Error::from(e))
        },
    }
}



async fn scan_for_api_key_usage(dynamo_client: &DynamoClient, api_key: &str) -> Result<String, Error> {
    let table_name = "ApiTable"; // Ensure this matches your actual table name
    let response = dynamo_client.scan()
        .table_name(table_name)
        .filter_expression("api_key = :api_key")
        .expression_attribute_values(":api_key", AttributeValue::S(api_key.to_string()))
        .send()
        .await?;

    let count = if let Some(items) = response.items.as_deref() {
        items.len() // If there are items, count them
    } else {
        0 // If there are no items, the count is 0
    };

    Ok(format!("API key {} has been used {} times.", api_key, count))
}
