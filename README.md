# OpenAI Story Generation and Query Counter Service

## Table of Contents
1. [Deliverables](#deliverables)
2. [Description](#description)
3. [Installation and Usage](#Installation and Usage)
4. [Development Process](#development-process)

## Deliverables

Proof of POST functionality with HTTP API of service (story is generated with OpenAI API):

![Story Screenshot](/Screenshot_2024-03-05_at_2.00.40_AM.png)

Proof of GET functionality with HTTP API of service (with database integration showing usage count)

![GET Screenshot](/Screenshot_2024-03-05_at_3.32.08_AM.png)

## Description

This Rust-based web service integrates with OpenAI's API for generating short stories and tracks API key usage in a DynamoDB table. The service is designed to handle HTTP requests using AWS Lambda framework. Upon receiving a request, the service determines the requested endpoint and performs the corresponding action. For the /generate-story endpoint, the service updates the API key usage count in the DynamoDB table and sends a request to the OpenAI API to generate a short story. If successful, it returns the generated story. For the /get-usage endpoint, the service retrieves the API key usage count from the DynamoDB table and returns the count as a response. The integration with DynamoDB allows for efficient tracking of API key usage, enabling administrators to monitor and manage usage effectively.


## Installation and Usage

To run this project locally, ensure Rust, Cargo, and Docker are installed on your system. Follow these installation steps:

1. Clone the repository:
git clone git@gitlab.com:dukeaiml/peterliuserverlessrust.git

2. Set up AWS configs. 
Do 

cargo lambda build --release --target x86_64-unknown-linux-musl

Then do 

cargo lambda deploy

3. Then try the endpoints 

Generate Story (POST /generate-story): Use this endpoint to generate short stories. Provide an API key as a query parameter.
Example: 

curl -X POST "https://bmppfc2ulntbkpqaubr6fakamq0jbyef.lambda-url.us-east-1.on.aws/generate-story?api_key=YOUR_API_KEY" -H "Content-Type: application/json" -d '{}'


Get API Key Usage (GET /get-usage): Retrieve API key usage information. Provide an API key as a query parameter.

curl -X GET "https://bmppfc2ulntbkpqaubr6fakamq0jbyef.lambda-url.us-east-1.on.aws/get-usage?api_key=YOUR_API_KEY"

## Development Process

Development steps included:

1. Environment Setup: Establishing Rust environment and dependencies.
2. Implementation: Writing code to integrate with OpenAI's API and DynamoDB.
3. Testing: Ensuring functionality and handling errors.
4. Documentation: Documenting code and writing this README.


